let $RUBYHOME=$HOME."/.rbenv/versions/2.5.1"
"set rubydll=$HOME/.rbenv/versions/2.5.1/lib/libruby.2.5.1.dylib
"set term=xterm-256color
call pathogen#runtime_append_all_bundles()
call pathogen#helptags()
filetype plugin indent on
filetype indent on
set t_Co=256
set background=dark
colorscheme molokai


set cindent
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
set number
set nonu
" only hide buffer when switching, perserves undo history
set hidden
syntax on 
map <F5> :w<CR>:make<CR>
map <F6> :w<CR>:!./test.sh<CR>
inoremap <F5> <ESC>:w<CR>:make<CR>
inoremap <F6> <ESC>:w<CR>:!./test.sh<CR>
map <F7> :tabp<CR>
map <F8> :tabn<CR>
map <F12> :cn<CR>
nnoremap <F4> :GundoToggle<CR>
nnoremap <F2> :TagbarToggle<CR>
nnoremap <F3> :NERDTreeToggle<CR>
let g:miniBufExplMapCTabSwitchBufs = 1 
let g:miniBufExplMapWindowNavVim = 1 
let g:miniBufExplMapWindowNavArrows = 1
map <C-W> <ESC>:w<CR>
inoremap <C-W> <ESC>:w<CR>
map <S-Left> :bp<CR>
map <S-Right> :bn<CR>
set rtp+=$GOROOT/misc/vim
" linting
nnoremap <c-N> :cp<CR>
nnoremap <c-n> :cn<CR>
autocmd FileType c,cpp,java,php,python autocmd BufWritePre <buffer> %s/\s\+$//e

autocmd FileType * execute 'setlocal dict+=~/.vim/words/'.&filetype.'.txt'

function SetJSOptions()
    setlocal shiftwidth=2 tabstop=2
    set formatprg=prettier-eslint\ --stdin
    inoremap <buffer><nowait> :: :{},<Left><Left>
    inoremap <buffer><nowait> :"" :"",<Left><Left>
    nnoremap <buffer><nowait> <Bslash>rnc :0r ~/.vim/templates/react-native-component.js<CR>
    nnoremap <buffer><nowait> <Bslash>rc :0r ~/.vim/templates/react-component.js<CR>
    nnoremap <c-k> :set mp=eslint_d\ --quiet\ --fix\ --cache\ --format=unix\ %:p \| make<CR><CR>:cn<CR>
    nnoremap <c-l> :set mp=eslint_d\ --fix\ --cache\ --format=unix\ %:p \| make<CR><CR>:cn<CR>
    set ft=javascript.jsx
endfunction

function SetPyOptions()
    nnoremap <c-l> :set mp=flake8\ %:p \| make<CR><CR>:cp<CR>
    nnoremap <c-s-l> :set mp=flake8\ %:p \| make<CR><ESC>
    nnoremap <buffer><nowait> <Bslash>fm :0r ~/.vim/templates/flask-model.py<CR>
    nnoremap <buffer><nowait> <Bslash>fb :0r ~/.vim/templates/flask-blueprint.py<CR>
endfunction

autocmd FileType javascript,javascriptreact,javascript.jsx call SetJSOptions()
autocmd FileType python call SetPyOptions()


" column length
set ruler
set cc=100
highlight OverLength ctermbg=darkred ctermfg=white guibg=#FFD9D9
match OverLength /\%>100v.\+/


" fix backspace
set backspace=2

" auto write on insert leave
set ttimeoutlen=5
autocmd TextChanged,InsertLeave * if &buftype == "" && !&readonly | write | endif

" emmet
let g:user_emmet_leader_key=','
let g:user_emmet_settings = webapi#json#decode(join(readfile(expand('~/.vim/emmet_snippets_custom.json')), "\n"))


let s:snippets_map={
  \ "fn": "() => {<c-o>",
  \ "fcx": "function XXX(props) {\<CR>return (\<CR><>\<CR>\<CR></>)}<c-o>3k",
  \ "uex": "useEffect(() => {<CR><Down><Left>, \[\]<Up><ESC>ddO<BS>",
  \ "ucx": "useCallback(() => {<CR><Down><Left>, \[\]<Up><ESC>ddO<BS>",
  \ "usx": "const [ , ] = useState\(<c-o>",
  \ "msx": "import { makeStyles } from '@material-ui/core/styles'\<CR>const useStyles = makeStyles({\<CR>})\<CR>const classes = useStyles()",
  \ "stx": "style={styles\.<c-o>",
  \ "cnx": "className={styles\.<c-o>",
  \ "pdbx": "import pdb;pdb.set_trace()<c-o>",
  \ "pxo": "(<c-o>x",
  \ "loggerx": "import logging\<CR>logger = logging.getLogger(__name__)\<CR>logger.setLevel(logging.DEBUG)",
  \ "cachex": "import pickle\<CR>with open('/tmp/performance_data.pkl', 'rb+') as f:\<CR>portfolio_chg, benchmark_chg = pickle.load(f)",
  \ "dfx": "display:'flex',\<CR>justifyContent:'center',\<CR>alignItems:'center',\<CR>flexFlow:'row wrap',"
  \ }

for [s:pattern, s:expansion] in items(s:snippets_map)
  execute "iabbrev" s:pattern s:expansion
endfor

" supertab
let g:SuperTabLongestEnhanced=1
let g:SuperTabLongestHighlight=1

"  ctrlp
set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.(git|hg|svn))|(node_modules)$',
  \ 'file': '\v\.(exe|so|dll)$',
\ }

" set title for iterm
set t_ts=]1;
set t_fs=
function! SetTerminalTitle()
    let titleString = expand('%:t')
    if len(titleString) > 0
        let &titlestring = expand('%:t')
        " this is the format iTerm2 expects when setting the window title
        let args = "\033];".&titlestring."\007"
        let cmd = 'silent !echo -e "'.args.'"'
        execute cmd
        redraw!
    endif
endfunction

autocmd BufEnter * call SetTerminalTitle()
let g:ac_last_line = getline(".")
" turn on auto complete if not on
function! MaybeTriggerAutoComplete()
    if !pumvisible() && getline(".")!=g:ac_last_line
        let g:ac_last_line = getline(".")
        if getline('.')[col('.')-2] =~'\K'
            call feedkeys("\<tab>")
        endif 
    endif
endfunction
autocmd TextChangedI * if &buftype == "" && ! &readonly | call MaybeTriggerAutoComplete() | endif
"Always Show Auto Complete
set completeopt=menu,menuone,noinsert,noselect

" macros
" in react, move styles to right after makeStyles({, replace style= to
" className=classes.
" minimi|zeBtn{position:"absolute", top: 3, right: 4, width: 40, height:40}
nnoremap <c-S> "adaw"bd:echo search('}')<CR>x/makeStyles(<CR>o<ESC>"apa:<ESC>"bpa},<ESC><c-o>iclasses.<ESC>"apBcwclassName<ESC>
" in react, auto fill useState and use callback
nnoremap <c-s><c-s> dawaconst [<ESC>pa, set<ESC>pblllgUlA] = useState()<ESC>i
inoremap <c-s><c-s> <ESC>dawaconst [<ESC>pa, set<ESC>pblllgUlA] = useState()<ESC>i
nnoremap <c-s><c-a> dawaconst <ESC>pa = ucx<c-]>
inoremap <c-s><c-a> <ESC>dawaconst <ESC>pa = ucx<c-]>  
" add styled component
noremap <c-s><c-d> yawgg/styled\.<CR><s-n>}oconst <ESC>pa = styled.div``<CR><up><end><left><CR><CR><up><space><space>
inoremap <c-s><c-d> <ESC><c-s><c-d>


"ctrl P
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'

set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.sdi     " MacOSX/Linux
let g:ctrlp_root_markers = ["Dockerfile", "package.json"]
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.git|\.hg|\.svn|node_modules|ios|android|build|vendor)$',
  \ 'file': '\v\.(exe|so|dll)$',
  \ }


set autoread
" convert an import to a lodable component imoport
let @l='dwiconst ��awwdwi=l�kb loadable	((�kr =?�kb> import (��a�kr�kr�krd$hP��a��a'
let @i='dwiimport ��awwdwdwifrom ��alx��aA�kb��a'

"autopair
let g:AutoPairsMapBS = 1
let g:AutoPairsFlyMode = 0
let g:AutoPairsCompatibleMaps = 0
let g:AutoPairsMoveExpression  = "<C-S-%key>"
" disable shorcuts that delayed ctrlp 
let g:AutoPairsShortcutJump = "" 
let g:AutoPairsShortcutToggleMultilineClose = ""
let g:AutoPairsShortcutToggle = ""

"shift backspace -> DEL
imap <C-D> <Del>

"folding
set foldmethod=indent
nnoremap <space> zczz
nnoremap <S-space> zozz
set foldlevelstart=99

" syntax highlighting
command! What echomsg synIDattr(synID(line('.'), col('.'), 1), 'name')
highlight def link jsxComponentName Function
highlight def link jsxEndComponentName Function
highlight def link jsObjectKey Label
highlight def link jsArrowFuncArgs Type
highlight def link jsFuncArgs Type
" fix an issue where redraw times out and disables syntax highlight
set redrawtime=10000
