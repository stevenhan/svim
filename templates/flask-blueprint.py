from marshmallow import ValidationError
from flask import Blueprint, jsonify, request, render_template
from apps.models.Study import Study, StudySchema
from apps.api.auth import get_user_from_request
from settings import db

endpoint = Blueprint('study_endpoint', __name__)
MODEL = Study
SCHEMA = StudySchema
UNIQUE_ID_KEY = 'id'

class InvalidPayload(Exception):
    def __init__(self, resp, status):
        self.resp = resp
        self.status = status

def _validate_payload():
    data = request.get_json()
    schema = SCHEMA()
    try:
        res = schema.load(data)
    except ValidationError as e:
        raise InvalidPayload(jsonify(e), 400)
    return res

@endpoint.app_errorhandler(InvalidPayload)
def invalid_response(e):
    return e.resp, e.status

@endpoint.route('/', methods=["POST"])
def create():
    user = get_user_from_request()
    data = _validate_payload()
    db.session.add(data)
    try:
        db.session.commit()
    except:
        return jsonify({"error": "Unique Study Name Required"})
    return jsonify({
        "success":True
    })

@endpoint.route('/<unique_id>', methods=["GET"])
def get(unique_id):
    user = get_user_from_request()
    res = MODEL.objects.filter_by(UNIQUE_ID_KEY=unique_id)
    if not res:
        return '', 404
    return schema().dump(res, many=True)

@endpoint.route('/', methods=["GET"])
def list():
    user = get_user_from_request()
    data = _validate_payload()
    schema = SCHEMA()
    res = MODEL.objects.all()
    return schema().dump(res, many=True)

@endpoint.route('/<unique_id>', methods=["PUT"])
def update(unique_id):
    data = _validate_payload()
    user = get_user_from_request()
    MODEL.objects.filter(UNIQUE_ID_KEY=unique_id).update(data)
    db.session.commit()
    return jonsify({
        "success":True
    })

@endpoint.route('/<unique_id>', methods=["DELETE"])
def delete(unique_id):
    user = get_user_from_request()
    MODEL.objects.filter(UNIQUE_ID_KEY=unique_id).delete()
    db.session.commit()
    return '', 200

