import datetime
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, ForeignKey, JSON, UniqueConstraint, DateTime, Boolean
from flask_admin.contrib.sqla import ModelView

from settings import ma, db

class Template(db.Model):
    __tablename__ = 'Template'
    __table_args__ = (
        UniqueConstraint(
            'user_id',
            'answer_id',
            name='unique_answer_per_user',
        ),
    )
    id = Column(Integer)
    user_id = Column(Integer, ForeignKey('user.id'), index=True)
    answer_id = Column(Integer, ForeignKey('answer.id'))
    # create relationsips to be used in python
    user = relationship("User")
    answer = relationship("QuizAnswer")

class TemplateModelView(ModelView):
    inline_models = ("QuizAnswer", )

class QuizAnswerSchema(ma.ModelSchema):
    class Meta:
        model = QuizAnswer

class TemplateSchema(ma.ModelSchema):
    answers = ma.Nested(QuizAnswerSchema, many=True)
    class Meta:
        model = QuizQuestion
