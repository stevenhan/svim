import React, { useCallback, useState, useEffect, useRef} from 'react';
import { View, TouchableWithoutFeedback } from 'react-native';
import { 
  Spinner, 
  withStyles, 
  Button, 
  CheckBox, 
  Icon, 
  Input, 
  StyleService, 
  Text, 
  useStyleSheet 
} from '@ui-kitten/components';
import LinearGradient from 'react-native-linear-gradient';
import {connect} from 'react-redux'
import ErrorText from '../../components/ErrorText'
import { useIsFocused } from '@react-navigation/native';
import { useAnimation } from 'react-native-animation-hooks'
import moment from 'moment'
import axios from 'moment'

const mapDispatch= {

}
const mapStateToProps = (state) => ({
  loading:state.user.signUpLoading
})

export default connect(mapStateToProps, mapDispatch)(withStyles(({
  loading, signUp, eva, navigation 
}) => {
  const styles = useStyleSheet(themedStyles);

  const [state, setState] = useState(  );
  const ref = useRef()
  const isFocused = useIsFocused();

  const callback = useCallback(() => {

  }, [])

  useEffect(() => {
  }, [])

  const expansion = useAnimation({
    type: 'spring',
    initialValue: 1,
    toValue: state? 0 : 1,
    bounciness:0,
    speed: 1,
    useNativeDriver: false,
  })

  return <>

  </>
}, theme=>({
  gradientStart: theme['color-primary-700'],
  gradientEnd: theme['color-primary-200'],
})))

const themedStyles = StyleService.create({
  container: {
    flex: 1,
    backgroundColor: 'background-basic-color-1',
    tintColor: 'text-hint-color',
  },
});
